function ad-tools {
    import-module activedirectory 
    $mainMenu = 'X'
    while($mainMenu -ne ''){
    Write-Host "`n `t Welcome to Simple AD Administration Script`n"-Foreground RED
    #Credit for ASCII ART https://www.asciiart.eu/computers/computers
    $block = @"
                                                 .------.------.    
  +-------------+                     ___        |      |      |    
  |             |                     \ /]       |      |      |    
  |             |        _           _(_)        |      |      |    
  |             |     ___))         [  | \___    |      |      |    
  |             |     ) //o          | |     \   |      |      |    
  |             |  _ (_    >         | |      ]  |      |      |    
  |          __ | (O)  \__<          | | ____/   '------'------'    
  |         /  o| [/] /   \)        [__|/_                          
  |             | [\]|  ( \         __/___\_____                    
  |             | [/]|   \ \__  ___|            |                   
  |             | [\]|    \___E/%%/|____________|_____              
  |             | [/]|=====__   (_____________________)             
  |             | [\] \_____ \    |                  |              
  |             | [/========\ |   |                  |              
  |             | [\]     []| |   |                  |              
  |             | [/]     []| |_  |                  |              
  |             | [\]     []|___) |                  |              
====================================================================

"@ 
    Write-Host $block -ForegroundColor Cyan
         $name = (Read-Host "Enter the username")
         $User = Get-ADUser -Filter {sAMAccountName -eq $name}
         If ($User -eq $Null) 
            {
              Write-Host "User does not exist in AD, please re-enter correct user name"
              ad-tools
            } 
         Else {
         End
         }
               
        clear-host

           $block = @"

             ________________________________________________
            /                                                \
           |    _________________________________________     |
           |   |                                         |    |
           |   |  C:\>Username to be edited is $name     |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |                                         |    |
           |   |_________________________________________|    |
           |                                                  |
            \_________________________________________________/
                   \___________________________________/
 
"@ 
    Write-Host $block -ForegroundColor white
        
        
        Write-Host -ForegroundColor Red "Please choose and option:"
        Write-Host -ForegroundColor DarkGray -NoNewline "`n["; Write-Host -NoNewline "1"; Write-Host -ForegroundColor DarkGray -NoNewline "]"; `
            Write-Host -ForegroundColor white " Unlock '$name' account"

        Write-Host -ForegroundColor DarkGray -NoNewline "`n["; Write-Host -NoNewline "2"; Write-Host -ForegroundColor DarkGray -NoNewline "]"; `
            Write-Host -ForegroundColor white " Reset '$name' password"

        Write-Host -ForegroundColor DarkGray -NoNewline "`n["; Write-Host -NoNewline "3"; Write-Host -ForegroundColor DarkGray -NoNewline "]"; `
            Write-Host -ForegroundColor white " Disable/Enable and reset password for account '$name' "

         
        $mainMenu = Read-Host "`nSelection (leave blank to quit)"
        # Launch submenu1
        if($mainMenu -eq 1){
            subMenu1
        }
        # Launch submenu2
        if($mainMenu -eq 2){
            subMenu2
        }
        # Launch submenu3
        if($mainMenu -eq 3){
            subMenu3
        }
    }
}

function subMenu1 {
             clear-host
             Unlock-Adaccount -identity $name
             $results = get-aduser -identity $name -property lockedout | select lockedout | foreach { $_.lockedout }
             
             $block = @"

              .........
            .'------.' |       Is the $name account locked: $results
           | .-----. | |
           | |     | | |
         __| |     | | |;. _______________
        /  |*`-----'.|.' `;              //
       /   `---------' .;'              //
 /|   /  .''''////////;'               //
|=|  .../ ######### /;/               //|
|/  /  / ######### //                //||
   /   `-----------'                // ||
  /________________________________//| ||
  `--------------------------------' | ||
   : | ||      | || |__LL__|| ||     | ||
   : | ||      | ||         | ||     `""'
   n | ||      `""'         | ||
   M | ||                   | ||
     | ||                   | ||
     `""'                   `""'
"@
        Write-Host $block -ForegroundColor Yellow
        $number = (Read-Host "Please enter 1 for main menu, 2 to quit")
                If ($number -eq 1){
                clear-host
                ad-tools
                } 
                elseif ($number -eq 2){
                clear-host
                exit 
                }
                                 
}

function subMenu2 {
            clear-host
            $password = (Read-host "Please enter the new password" -assecurestring)         
            Set-ADAccountPassword -identity $name -NewPassword $password        
            $results = get-aduser -identity $name -property passwordlastset | foreach { $_.passwordlastset }
            $block = @"

              .........
            .'------.' |       When was the password last set for account $name : $results
           | .-----. | |
           | |     | | |
         __| |     | | |;. _______________
        /  |*`-----'.|.' `;              //
       /   `---------' .;'              //
 /|   /  .''''////////;'               //
|=|  .../ ######### /;/               //|
|/  /  / ######### //                //||
   /   `-----------'                // ||
  /________________________________//| ||
  `--------------------------------' | ||
   : | ||      | || |__LL__|| ||     | ||
   : | ||      | ||         | ||     `""'
   n | ||      `""'         | ||
   M | ||                   | ||
     | ||                   | ||
     `""'                   `""'
"@
      Write-Host $block -ForegroundColor Green
      $number = (Read-Host "Please enter 1 for main menu, 2 to quit")
               If ($number -eq 1){
               clear-host
               ad-tools
               } 
               elseif ($number -eq 2){
               clear-host
               exit 
               }                    
                      
}



function submenu3 {
    $subMenu3 = 'X'
    while($subMenu3 -ne ''){
        Clear-Host
        Write-Host "Disable/Enable account $name"
        Write-Host -ForegroundColor Red "Choose an option"
        Write-Host -ForegroundColor DarkGray -NoNewline "`n["; Write-Host -NoNewline "1"; Write-Host -ForegroundColor DarkGray -NoNewline "]"; `
            Write-Host -ForegroundColor Green " Disable $name account"
        Write-Host -ForegroundColor DarkGray -NoNewline "`n["; Write-Host -NoNewline "2"; Write-Host -ForegroundColor DarkGray -NoNewline "]"; `
            Write-Host -ForegroundColor Green " Enable $name account"

        $subMenu3 = Read-Host "`nSelection (leave blank to quit)"

        # Option 1
        if($subMenu3 -eq 1){
            clear-host
            Disable-ADAccount -Identity $name
            $results = get-aduser -identity $name -property enabled | foreach { $_.enabled }
            $block = @"

              .........
            .'------.' |       Is the account $name enabled: $results
           | .-----. | |
           | |     | | |
         __| |     | | |;. _______________
        /  |*`-----'.|.' `;              //
       /   `---------' .;'              //
 /|   /  .''''////////;'               //
|=|  .../ ######### /;/               //|
|/  /  / ######### //                //||
   /   `-----------'                // ||
  /________________________________//| ||
  `--------------------------------' | ||
   : | ||      | || |__LL__|| ||     | ||
   : | ||      | ||         | ||     `""'
   n | ||      `""'         | ||
   M | ||                   | ||
     | ||                   | ||
     `""'                   `""'
"@
          Write-Host $block -ForegroundColor Red
          $number = (Read-Host "Please enter 1 for main menu, 2 to quit, 3 previous")
                If ($number -eq 1){
                clear-host
                ad-tools
                } 
                elseif ($number -eq 2){
                clear-host
                exit 
                }
                elseif ($number -eq 3){
                clear-host
                submenu3
                }
            
        }


        # Option 2
        if($subMenu3 -eq 2){
            clear-host
            Enable-ADAccount -Identity $name
            $results = get-aduser -identity $name -property enabled | foreach { $_.enabled }
            $password = (Read-host "Please enter the new password" -assecurestring)         
            Set-ADAccountPassword -identity $user -NewPassword $password
            $results2 = get-aduser -identity $user -property passwordlastset | foreach { $_.passwordlastset }
            $block = @"

              .........
            .'------.' |       Is the account $name enabled: $results
           | .-----. | |       When was the password last set: $results2
           | |     | | |
         __| |     | | |;. _______________
        /  |*`-----'.|.' `;              //
       /   `---------' .;'              //
 /|   /  .''''////////;'               //
|=|  .../ ######### /;/               //|
|/  /  / ######### //                //||
   /   `-----------'                // ||
  /________________________________//| ||
  `--------------------------------' | ||
   : | ||      | || |__LL__|| ||     | ||
   : | ||      | ||         | ||     `""'
   n | ||      `""'         | ||
   M | ||                   | ||
     | ||                   | ||
     `""'                   `""'
"@
          Write-Host $block -ForegroundColor Cyan
          $number = (Read-Host "Please enter 1 for main menu, 2 to quit, 3 prev menu")
                If ($number -eq 1){
                clear-host
                ad-tools
                } 
                elseif ($number -eq 2){
                clear-host
                exit 
                }
                elseif ($number -eq 3){
                clear-host
                submenu3
                }
        }
        

    }
}

ad-tools


